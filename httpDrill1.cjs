const http = require("http");
const fs = require("fs");
const { v4: uuidv4 } = require('uuid');
const PORT =  process.env.PORT || 8000;

function readFile(path){
    return new Promise((resolve, reject) => {
        fs.readFile(path, "utf-8", (err, data) => {
            if(err){
                reject(err);
            }
            else {
                resolve(data);
            }
        });
    });
}

function requestListener(request, response){
    
    if (request.url === "/html" && request.method === "GET"){
        readFile('index.html')
        .then((data) => { 
            response.writeHead(200, {"Content-Type" : "text/html"});
            response.write(data);
            response.end();
        })
        .catch((err) => {
            console.error(err);
            response.writeHead(500, {"Content-Type" : "application/json"});
            response.end(JSON.stringify({
                "error" : "reading html file"
            }));
        });
    }

    else if (request.url === "/json" && request.method === "GET"){
        readFile('data.json')
        .then((data) => {
            response.writeHead(200, {"Content-Type" : "application/json"});
            response.write(data);
            response.end();
        })
        .catch((err) => {
            console.error(err);
            response.writeHead(500, {"Content-Type" : "application/json"});
            response.end(JSON.stringify({
                "error" : "reading json data file"
            }));
        });
    }

    else if (request.url === "/uuid" && request.method === "GET"){
        let randomUUID = {
            uuid : uuidv4()
        };
        response.writeHead(200, {"Content-Type" : "application/json"});
        response.write(JSON.stringify(randomUUID));
        response.end();
    }

    else if (request.url.startsWith("/status") && request.url.split("/").length === 3 && request.method === "GET"){

        let statusCode = request.url.split("/").slice(-1);
        let statusMsg = {};
        if(statusCode in http.STATUS_CODES){    
            statusMsg = {
                [statusCode] : http.STATUS_CODES[statusCode]
            };
        } 
        else {
            statusCode = 404;
            statusMsg = {
                "error" : "Not a valid http status code"
            }
        }   
        response.writeHead(statusCode, {"Content-Type" : "application/json"}); 
        response.write(JSON.stringify(statusMsg));
        response.end(); 
    } 

    else if (request.url.startsWith("/delay") && request.url.split("/").length === 3 && request.method === "GET"){
        
        let delay = Number(request.url.split("/").slice(-1));

        if(isNaN(delay) || delay < 0){
            response.writeHead(400, {"Content-Type" : "application/json"});
            response.end(JSON.stringify({
                "error" : "Not a valid time"
            }));
        }
        else {
            setTimeout(() => {
                response.writeHead(200, {"Content-Type" : "application/json"});        
                response.write(JSON.stringify({
                    "delay" : `${delay} seconds`
                }));
                response.end();
            }, delay * 1000);
        }     
    }

    else {
        response.writeHead(404, {"Content-Type" : "application/json"});
        response.write(JSON.stringify({
            404 : http.STATUS_CODES[404]
        }));
        response.end();
    }
}

const server = http.createServer(requestListener);
server.listen(PORT, () => {
    console.log(`Server is listening on port: ${PORT}`);
});